from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, include
from productos import urls as url_products
from usuarios import urls as url_users

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(url_products)),
    path('', include(url_users)),
    #path('/', admin.site.urls),
    


]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
