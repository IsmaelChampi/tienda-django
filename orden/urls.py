from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, include

from django.urls import path


# View
from users import views


urlpatterns = [

    # Posts
    path(
        route='<str:username>/',
        view=view.UserDetailView.as_view(),
        name=''
    ),

    # Management
    path(
        route='order//',
        view=views.login_view,
        name=''
    ),
    path(
        route='order//',
        view=views.logout_view,
        name=''
    ),
    path(
        route='order/signup/',
        view=views.signup,
        name=''
    ),
    path(
        route='order/me/profile/',
        view=views.update_profile,
        name=''
    )

]