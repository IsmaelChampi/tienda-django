from django.contrib import admin

from productos.models import Products
# Register your models here.


@admin.register(Products)
class ProductsAdmin(admin.ModelAdmin):
    list_display = ('nombre_product', 'sku')
    list_per_page = 30