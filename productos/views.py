from django.shortcuts import render, redirect

from productos.forms import productForm


from productos.models import Products

# Creacion vista product_profile para ingresar datos
def products_new(request):
    if request.method == 'POST':
        #form = profileForm(request.POST, instance=request.user)  ##para actualizar un objeto en la base de datos
        form = productForm(request.POST)  #para crear un nuevo objeto en la base
        if form.is_valid():
            form.save()

            #new_user = Perfil.objects.create(first_name=data['nombrecompleto'], last_name=data['apellido'], direccion = data['direc'])

            return redirect('home')

    else:
    	form = productForm() #para crer un nuevo objeto
        #form = profileForm(instance=request.user)   ##para actualizar un objeto en la base de datos


    return render(
        request=request,
        template_name='products_new.html',
        context={
            #'profile': profile,
            'form': form
        })



#vista home 
def home_product(request):


	return render(request,'home.html')



#listar productos en el template
def list_product(request):

    products = Products.objects.all()

    return render(
        request=request,
        template_name='list_product.html',
        context={
            'products': products,
          
        })


#detalles de producto en el template
def Detalle_product(request, pk):


    product = Products.objects.get(pk=pk)


    return render(
        request=request,
        template_name='Detalle_product.html',
        context={
            'product': product,
          
        })
	
   