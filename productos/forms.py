from django import forms
from productos.models import Products

from django.utils.translation import ugettext as _
class productForm(forms.ModelForm):
    
    #Formulario productos

    class Meta:
        model = Products
        fields = ('nombre_product','sku', 'price', 'picture', 'description',)