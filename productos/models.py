from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext as _

# Create your models productos.


#Modelo base de datos Productos
class Products(models.Model):
    """ Modelo de productos de usuario """
    nombre_product = models.TextField(
    verbose_name=('NombreProducto'), 
    max_length=100, 
    blank=True, null=True )


    sku = models.IntegerField(
    verbose_name=('Codigobarra'), 
    blank=True, null=True)
    

    price = models.IntegerField(
    verbose_name=('Precio'), 
    blank=True, null=True)
    

    picture = models.ImageField(
    	verbose_name=('Picture'),
        upload_to='pictures',
        blank=True,
        null=True
    )


    description = models.TextField(
    verbose_name=('Descripcion'),
    blank=True)


    date = models.DateTimeField(auto_now_add=True)


    #class Meta:
       # verbose_name = _('Perfil')
       # verbose_name_plural = _('Perfiles')