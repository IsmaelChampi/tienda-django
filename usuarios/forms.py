from django import forms
from usuarios.models import Perfil

from django.utils.translation import ugettext as _
class PerfilForm(forms.ModelForm):
    first_name = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': _('Nombre(s)'), 'class': "form-control"}),
        max_length=80,
        error_messages={'required': _(u'Ingrese un nombre válido')},
        label=_('Nombre(s)*'),
        required=True,
    ),

    last_name = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': _('Apellidos(s)'), 'class': "form-control"}),
        max_length=80,
        error_messages={'required': _(u'Ingrese un apellido válido')},
        label=_('Apellidos(s)*'),
        required=True,
    )

    class Meta:
        model = Perfil
        fields = ('username','first_name', 'last_name', 'direccion', 'sexo', 'codigo', 'edad', 'estado', 'email', 'telefono', )