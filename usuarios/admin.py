from django.contrib import admin

from usuarios.models import Perfil
# Register your models here.


@admin.register(Perfil)
class PerfilAdmin(admin.ModelAdmin):
    list_display = ('username', 'first_name')
    list_per_page = 30
