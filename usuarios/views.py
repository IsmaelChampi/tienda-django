from django.shortcuts import render, redirect

from usuarios.forms import PerfilForm


from usuarios.models import Perfil

# Creacion vista usuer para ingresar datos del usuario
def user_new(request):
    if request.method == 'POST':
        #form = profileForm(request.POST, instance=request.user)  ##para actualizar un objeto en la base de datos
        form = PerfilForm(request.POST)  #para crear un nuevo objeto en la base
        if form.is_valid():
            form.save()

            #new_user = Perfil.objects.create(first_name=data['nombrecompleto'], last_name=data['apellido'], direccion = data['direc'])

            return redirect('home')

    else:
    	form = PerfilForm() #para crer un nuevo objeto
        #form = profileForm(instance=request.user)   ##para actualizar un objeto en la base de datos


    return render(
        request=request,
        template_name='user_new.html',
        context={
            #'profile': profile,
            'form': form
        })


#listar usuarios en el template
def list_user(request):

    perfil = Perfil.objects.all()

    return render(
        request=request,
        template_name='list_user.html',
        context={
            'perfil': perfil,
          
        })

#detalles de usuario en el template
def Detalle_user(request, pk):


    perfil = Perfil.objects.get(pk=pk)


    return render(
        request=request,
        template_name='Detalle_user.html',
        context={
            'perfil': perfil,
          
        })


	
   


